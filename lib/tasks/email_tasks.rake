desc 'lending expires lender'
task lending_expires_lender: :environment do
  # ... set options if any
  Lending.all.each do |lending|
    timespan = lending.end - DateTime.now
    if (timespan.to_f >= 13.5) && (timespan.to_f <= 14.5) #two weeks to end
      LenderMailer.lending_expires_lender(lending).deliver!
      EmployeeMailer.lending_expires_employee(lending).deliver!
    end
    if(timespan.to_f >= 0.5) && (timespan.to_f <= 1.5)
      LenderMailer.lending_expires_lender(lending).deliver!
      EmployeeMailer.lending_expires_employee(lending).deliver!
    end
      EmployeeMailer.lending_expires_employee(lending).deliver!
  end
end