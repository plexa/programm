require 'test_helper'

class EmployeeTest < ActiveSupport::TestCase
  def setup
    @Employee = Employee.new(name: "Example Employee", email: "employees@example.com", lab:"technik" )
  end

  test "should be valid" do
    assert @Employee.valid?
  end
  
  test "name should be present" do
    @Employee.name = "     "
    assert_not @employees.valid?
  end
  
  test "email should be present" do
    @Employee.email = "     "
    assert_not @Employee.valid?
  end
  
  test "name should not be too long" do
    @Employee.name = "a" * 51
    assert_not @Employee.valid?
  end

  test "email should not be too long" do
    @Employee.email = "a" * 244 + "@example.com"
    assert_not @Employee.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @Employee.email = valid_address
      assert @Employee.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end
end
