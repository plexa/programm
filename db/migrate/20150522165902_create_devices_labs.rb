class CreateDevicesLabs < ActiveRecord::Migration
  def change
    create_table :devices_labs do |t|
      t.integer :device_id
      t.integer :lab_id

      t.timestamps null: false
    end
  end
end
