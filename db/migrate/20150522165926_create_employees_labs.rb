class CreateEmployeesLabs < ActiveRecord::Migration
  def change
    create_table :employees_labs do |t|
      t.integer :employee_id
      t.integer :lab_id

      t.timestamps null: false
    end
  end
end
