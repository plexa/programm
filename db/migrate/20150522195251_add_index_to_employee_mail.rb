class AddIndexToEmployeeMail < ActiveRecord::Migration
  def change
  add_index :employees, :mail, unique: true
  end
end
