class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :name
      t.text :description
      t.integer :lab_id

      t.timestamps null: false
    end
  end
end
