class CreateLendings < ActiveRecord::Migration
  def change
    create_table :lendings do |t|
      t.date :start
      t.date :end
      t.integer :device_id
      t.integer :lab_id
      t.integer :employee_id
      t.integer :lender_id

      t.timestamps null: false
    end
  end
end
