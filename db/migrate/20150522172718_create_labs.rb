class CreateLabs < ActiveRecord::Migration
  def change
    create_table :labs do |t|
      t.string :name
      t.integer :device_id
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
