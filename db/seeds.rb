# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#User.create!(name:  "Example User",
#             email: "example@railstutorial.org",
#             password:              "foobar",
#             password_confirmation: "foobar",
#             admin: true)
#
#99.times do |n|
#  name  = Faker::Name.name
#  email = "example-#{n+1}@railstutorial.org"
#  password = "password"
#  User.create!(name:  name,
#               email: email,
#               password:              password,
#               password_confirmation: password)
#end

Lab.create(name: "Kasimir Labor")

#Device.create(name: "device",
#            description: "test",
#            lab_id: 1)
            
#Lender.create(name: "tilo",
#            mail: "tilo.zuelske@fh-stralsund.de")

Employee.create(name: "Admin",
            mail: "admin@mail.de",
            password: "rootroot",
            password_confirmation: "rootroot",
            lab_id: "1",
            admin: true)
            
#Lending.create(start: "2015-06-28",
#            end:"2015-06-29",
#            device_id: 1,
#            lab_id: 1,
#            employee_id: 1,
#            lender_id: 1)