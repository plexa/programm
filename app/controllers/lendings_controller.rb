class LendingsController < ApplicationController
  before_action :set_lending, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_employee
    
    
  
  # GET /lendings
  # GET /lendings.json
  def index
    @lendings = Lending.all
    @devices = Device.all.select{|device| device.lab == current_employee.lab}
    @lendings_devices = Array.new()
    Lending.all.each do |lending|
      @lendings_devices << lending.device
    end
    @device_options = @devices - @lendings_devices
    @lender_options = Lender.all
  end

  # GET /lendings/1
  # GET /lendings/1.json
  def show
  end
  # GET /lendings/new
  def new
    @lending = Lending.new
    
    @devices = Device.all.select{|device| device.lab == current_employee.lab}
    @lendings_devices = Array.new()
    Lending.all.each do |lending|
      @lendings_devices << lending.device
    end
    @device_options = @devices - @lendings_devices
    @device_options = @device_options.map{|g| [g.name, g.id]}
    
    @lender_options = Lender.all.map{|l| [ l.name, l.id ] }
    @lending = Lending.new
    
    @lending.start = Date.today
    @lending.end = Date.tomorrow
  end

  # GET /lendings/1/edit
  def edit
    @device_options = Device.all.map{|g| [g.name, g.id]}
    Lending.all.each do |lending|
      @device_options.delete([lending.device.name, lending.device_id])
    end
    @device_options.insert(0, [@lending.device.name, @lending.device.id])
    @lender_options = Lender.all.map{|l| [ l.name, l.id ] }
  end

  # POST /lendings
  # POST /lendings.json
  def create
    @lending = Lending.new(lending_params)
    @lending.employee = current_employee
   #@lending.lab =  Lab.find(params[:lab_id])
    @lending.lab = current_employee.lab
    @lending.device = Device.find(params[:device_id])
    @lending.lender = Lender.find(params[:lender_id])
    
    if @lending.lender == nil
      redirect_to lender_new
    end

    respond_to do |format|
      if @lending.save
        format.html { redirect_to @lending, notice: 'Lending was successfully created.' }
        format.json { render :show, status: :created, location: @lending }
      else
        format.html { render :new }
        format.json { render json: @lending.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lendings/1
  # PATCH/PUT /lendings/1.json
  def update
    @lending.employee = current_employee
   #@lending.lab =  Lab.find(params[:lab_id])
    @lending.lab = current_employee.lab
    @lending.device = Device.find(params[:device_id])
    @lending.lender = Lender.find(params[:lender_id])
    
    respond_to do |format|
      if @lending.update(lending_params)
        format.html { redirect_to @lending, notice: 'Lending was successfully updated.' }
        format.json { render :show, status: :ok, location: @lending }
      else
        format.html { render :edit }
        format.json { render json: @lending.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lendings/1
  # DELETE /lendings/1.json
  def destroy
    @lending.destroy
    respond_to do |format|
      format.html { redirect_to lendings_url, notice: 'Lending was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lending
      @lending = Lending.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lending_params
      params.require(:lending).permit(:start, :end, :device_id, :lab_id, :employee_id, :lender_id)
    end
    
    
    
end
