class LabsController < ApplicationController
  before_action :set_lab, only: [:show, :edit, :update, :destroy]
    before_action :logged_in_employee
    before_action :logged_in_admin
    

  # GET /labs
  # GET /labs.json
  def index
    @labs = Lab.all
  end

  # GET /labs/1
  # GET /labs/1.json
  def show
  end

  # GET /labs/new
  def new
    @lab = Lab.new
  end

  # GET /labs/1/edit
  def edit
  end

  # POST /labs
  # POST /labs.json
  def create
    @lab = Lab.new(lab_params)

    respond_to do |format|
      if @lab.save
        format.html { redirect_to @lab, notice: 'Lab was successfully created.' }
        format.json { render :show, status: :created, location: @lab }
      else
        format.html { render :new }
        format.json { render json: @lab.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /labs/1
  # PATCH/PUT /labs/1.json
  def update
    respond_to do |format|
      if @lab.update(lab_params)
        format.html { redirect_to @lab, notice: 'Lab was successfully updated.' }
        format.json { render :show, status: :ok, location: @lab }
      else
        format.html { render :edit }
        format.json { render json: @lab.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /labs/1
  # DELETE /labs/1.json
  def destroy
    @has_active_lendings = false
    Lending.all.each do |lending|
      @has_active_lendings ||= lending.lab == @lab
    end
    
    @has_active_employees = false
    Employee.all.each do |employee|
      @has_active_employees ||= employee.lab == @lab
    end
    
    @has_active_devices = false
    Device.all.each do |device|
      @has_active_devices ||= device.lab == @lab
    end
    
    if !@has_active_lendings && !@has_active_employees && !@has_active_devices
      @lab.destroy
      respond_to do |format|
        format.html { redirect_to labs_url, notice: 'Lab was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to labs_url, notice: 'Lab cannot be destroyed as long as it has lendings, employees or devices!' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lab
      @lab = Lab.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lab_params
      params.require(:lab).permit(:name)
    end
end
