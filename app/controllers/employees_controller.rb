class EmployeesController < ApplicationController
  before_action :logged_in_employee
  before_action :logged_in_admin
  before_action :correct_employee,   only: [:edit, :update]
  before_action :set_employee,       only: [:show, :edit, :update, :destroy]
  before_action :admin_employee,     only: :destroy

  # GET /employees
  # GET /employees.json
  def index
    @employees = Employee.all
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
   @employees = Employee.find(params[:id])
  end

  # GET /employees/new
  def new
    @lab_options = Lab.all.map{|l| [ l.name, l.id ] }
    @employee = Employee.new
  end

  # GET /employees/1/edit
  def edit
    @lab_options = Lab.all.map{|l| [ l.name, l.id ] }
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)
    @employee.lab =  Lab.find(params[:lab_id])

    respond_to do |format|
      if @employee.save
        #log_in @employee
        flash[:success] = "Welcome to the DeviceTracker App!"
        format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
        
        format.json { render :show, status: :created, location: @employee }
      else
        @lab_options = Lab.all.map{|l| [ l.name, l.id ] }
        format.html { render :new , notice: 'test' }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    @lab_options = Lab.all.map{|l| [ l.name, l.id ] }
    @employee.lab =  Lab.find(params[:lab_id])
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @has_active_lendings = false
    Lending.all.each do |lending|
      @has_active_lendings ||= lending.employee == @employee
    end
    
    if !@has_active_lendings
      @employee.destroy
      respond_to do |format|
        format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to employees_url, notice: 'Employee cannot be destroyed as long as he/she has lendings!' }
        format.json { head :no_content }
      end
    end
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:name, :mail, :lab_id, :password, :password_confirmation)
    end
    
    # Confirms the correct employee.
    def correct_employee
      @employee = Employee.find(params[:id])
      #redirect_to(root_url) unless current_employee?(@employee)
    end
    
    
end