class SessionsController < ApplicationController
  def new
  end

  def create
    employee = Employee.find_by(mail: params[:session][:mail].downcase)
    if employee && employee.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in employee
      params[:session][:remember_me] == '1' ? remember(employee) : forget(employee)
      remember employee
      redirect_back_or employee
    else
       flash.now[:danger] = 'Invalid email/password combination'
      # Create an error message.
      render 'new'
    end
  end

  def current_employee
    @current_employee ||= Employee.find_by(id: session[:employee_id])
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
  
  def log_out
    forget(current_employee)
    session.delete(:employee_id)
    @current_employee = nil
  end

end
