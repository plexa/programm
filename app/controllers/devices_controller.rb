class DevicesController < ApplicationController
  before_action :set_device, only: [:show, :edit, :update, :destroy]
    before_action :logged_in_employee

  # GET /devices
  # GET /devices.json
  def index
    @devices = Device.all
  end

  # GET /devices/1
  # GET /devices/1.json
  def show
  end

  # GET /devices/new
  def new
    @lab_options = Lab.all.map{|l| [ l.name, l.id ] }
    @device = Device.new
  end

  # GET /devices/1/edit
  def edit
    @description = @device.description
  end

  # POST /devices
  # POST /devices.json
  def create
    @device = Device.new(device_params)
    @device.lab =  current_employee.lab
    @lab_options = Lab.all.map{|l| [ l.name, l.id ] }

    respond_to do |format|
      if @device.save
        format.html { redirect_to @device, notice: 'Device was successfully created.' }
        format.json { render :show, status: :created, location: @device }
      else
        format.html { render :new }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /devices/1
  # PATCH/PUT /devices/1.json
  def update
    @description = @device.description
    respond_to do |format|
      if @device.update(device_params)
        format.html { redirect_to @device, notice: 'Device was successfully updated.' }
        format.json { render :show, status: :ok, location: @device }
      else
        format.html { render :edit }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /devices/1
  # DELETE /devices/1.json
  def destroy
    @has_active_lendings = false
    Lending.all.each do |lending|
      @has_active_lendings ||= lending.device == @device
    end
    
    if !@has_active_lendings
      @device.destroy
      respond_to do |format|
        format.html { redirect_to devices_url, notice: 'Device was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to devices_url, notice: 'Device cannot be destroyed as long as it has lendings!' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_device
      @device = Device.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def device_params
      params.require(:device).permit(:name, :description, :lab_id)
    end
end
