class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  
  def admin_employee
    current_employee.admin?
  end
  
  def logged_in_admin
    unless current_employee.admin?
      store_location
      flash[:danger] = "Only admins can do that."
      redirect_to root_url
    end
  end
  
  def logged_in_employee
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    
  
end
