json.array!(@employees) do |employee|
  json.extract! employee, :id, :name, :mail, :lab_id
  json.url employee_url(employee, format: :json)
end
