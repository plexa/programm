json.array!(@lendings) do |lending|
  json.extract! lending, :id, :start, :end, :device_id, :lab_id, :employee_id, :lender_id
  json.url lending_url(lending, format: :json)
end
