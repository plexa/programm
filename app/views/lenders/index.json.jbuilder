json.array!(@lenders) do |lender|
  json.extract! lender, :id, :name, :mail
  json.url lender_url(lender, format: :json)
end
