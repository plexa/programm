class DateValidator < ActiveModel::Validator
  def validate(record)
    if record.end <= record.start
      record.errors[:base] << "End date must be later than start"
    end
  end
end

class Lending < ActiveRecord::Base
    belongs_to :lender
    belongs_to :device
    belongs_to :lab
    belongs_to :employee
    
    
    

  def lender_name
    lender.name if lender
  end

  def lender_name=(name)
    self.lender = Lender.find_or_create_by_name(name) unless name.blank?
  end
  
  validates_with DateValidator
end


