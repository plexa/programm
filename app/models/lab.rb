class Lab < ActiveRecord::Base
    validates :name, presence: true ,length: { maximum: 50 },uniqueness: { case_sensitive: false }
        
    
    has_many :employees
    has_many :devices
end
