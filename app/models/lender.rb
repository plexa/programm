class Lender < ActiveRecord::Base
    before_save { self.mail = mail.downcase }
    validates :name, presence: true ,length: { maximum: 50 }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :mail, presence: true ,length: { maximum: 200 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }
    
    has_one :lending
end
