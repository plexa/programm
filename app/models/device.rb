class Device < ActiveRecord::Base
    
    
    has_one :lending
    
    belongs_to :lab
    has_many :employees, through: :lab
    
    validates :name, presence: true ,length: {maximum: 50 },uniqueness: { case_sensitive: false }
end
