class Employee < ActiveRecord::Base
    attr_accessor :remember_token   
    before_save { self.mail = mail.downcase }
    validates :name, presence: true ,length: { maximum: 50 }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :mail, presence: true ,length: { maximum: 200 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }
    has_secure_password
    validates :password, length: { minimum: 8 }
    validates :lab, presence: true 
   
   def Employee.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
   end
   
   # Returns a random token.
  def Employee.new_token
    SecureRandom.urlsafe_base64
  end
   
  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = Employee.new_token
    update_attribute(:remember_digest, Employee.digest(remember_token))
  end
  
   # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
   # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
  
    has_many :lendings
    
    belongs_to :lab
    has_many :devices, through: :lab
end
