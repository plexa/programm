class EmployeeMailer < ApplicationMailer
     default from: 'DeviceTracker-Team'
 
  def lending_expires_employee(lending)
    @url  = 'https://team-arbeit-plexa.c9.io'
    @days = (lending.end - DateTime.now).to_i
    @lending = lending
    
    mail(to: lending.employee.mail, subject: "In " + @days.to_s + " days the Lending of " + lending.device.name + " to " + lending.lender.name + " expires")
  end
end