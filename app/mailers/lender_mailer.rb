class LenderMailer < ApplicationMailer
     default from: 'DeviceTracker-Team'
 
  def lending_expires_lender(lending)
    @lender = lending.lender
    @url  = 'https://team-arbeit-plexa.c9.io'
    @employee = lending.employee
    
    mail(to: lending.lender.mail, subject: 'In X Weeks your Landing of DeviceY expires')
  end
end
